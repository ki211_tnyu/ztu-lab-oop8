﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarytask1
{
    public class Student: Person
    {
        private int course;
        public int Course
        {
            set { course = value; }
            get { return course; }
        }
        private string group;
        public string Group
        {
            set { group = value; }
            get { return group; }
        }
        private string facultet;
        public string Facultet
        {
            set { facultet = value; }
            get { return facultet; }
        }
        private string university;
        public string University
        {
            set { university = value; }
            get { return university; }
        }
        public Student() : base()
        {
            Course = 1;
            Group = "Unknown";
            Facultet = "Unknown";
            University = "Unknown";
        }
        public Student(string name, string surname, int year, int month, int day, int
       course, string group, string facultet, string university) : base(name, surname, year,
       month, day)
        {
            Course = course;
            Group = group;
            Facultet = facultet;
            University = university;
        }
        public Student(string name, string surname, int year, int month, int day,
       string group, string facultet) : base(name, surname, year, month, day)
        {
            Course = 1;
            Group = group;
            Facultet = facultet;
            University = "ZTU";
        }
        public Student(Student obj)
        {
            Course = obj.Course;
            Group = obj.Group;
            Facultet = obj.Facultet;
            University = obj.University;
        }
        public override void ReadInfo()
        {
            base.ReadInfo();
            int tmp = 0;
            Console.Write("Введіть курс:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть курс:");
            }
            Course = tmp;
            Console.Write("Введіть групу:"); Group = Console.ReadLine();
            Console.Write("Введіть факультет:"); Facultet = Console.ReadLine();
            Console.Write("Введіть університет:"); University = Console.ReadLine();
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"\t\tКурс:\t\t{Course}");
            Console.WriteLine($"\t\tГрупа:\t\t{Group}");
            Console.WriteLine($"\t\tФакультет:\t\t{Facultet}");
            Console.WriteLine($"\t\tУніверситет:\t\t{University}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("---------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}


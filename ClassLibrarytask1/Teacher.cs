﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarytask1
{
    public class Teacher: Person
    {
        private string posada;
        public string Posada
        {
            set { posada = value; }
            get { return posada; }
        }
        private string kafedra;
        public string Kafedra
        {
            set { kafedra = value; }
            get { return kafedra; }
        }
        private string university;
        public string University
        {
            set { university = value; }
            get { return university; }
        }
        public Teacher() : base()
        {
            Posada = "Unknown";
            Kafedra = "Unknown";
            University = "Unknown";
        }
        public Teacher(string name, string surname, int year, int month, int day,
       string posada, string kafedra, string university) : base(name, surname, year, month,
       day)
        {
            Posada = posada;
            Kafedra = kafedra;
            University = university;
        }
        public Teacher(string name, string surname, int year, int month, int day,
       string posada, string kafedra) : base(name, surname, year, month, day)
        {
            Posada = posada;
            Kafedra = kafedra;
            University = "ZTU";
        }
        public Teacher(Teacher obj)
        {
            Posada = obj.Posada;
            Kafedra = obj.Kafedra;
            University = obj.University;
        }
        public override void ReadInfo()
        {
            base.ReadInfo();
            Console.Write("Введіть посаду:"); Posada = Console.ReadLine();
            Console.Write("Введіть кафедру:"); Kafedra = Console.ReadLine();
            Console.Write("Введіть університет:"); University = Console.ReadLine();
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"\t\tПосада:\t\t{Posada}");
            Console.WriteLine($"\t\tКафедра:\t\t{Kafedra}");
            Console.WriteLine($"\t\tУніверситет:\t\t{University}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("---------------------------------------------------------------------------------------");            
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarytask1
{
    public class Reader: Person
    {
        private int ticket;
        public int Ticket
        {
            set { ticket = value; }
            get { return ticket; }
        }
        private int day_book;
        public int Day_book
        {
            set { day_book = value; }
            get { return day_book; }
        }
        private int month_book;
        public int Month_book
        {
            set { month_book = value; }
            get { return month_book; }
        }
        private int year_book;
        public int Year_book
        {
            set { year_book = value; }
            get { return year_book; }
        }
        private double payment;
        public double Payment
        {
            set { payment = value; }
            get { return payment; }
        }
        public Reader() : base()
        {
            Ticket = 1;
            Day_book = 1;
            Month_book = 1;
            Year_book = 1999;
            Payment = 50;
        }
        public Reader(string name, string surname, int year, int month, int day, int
       ticket, int day_book, int month_book, int year_book, double payment) : base(name,
       surname, year, month, day)
        {
            Ticket = ticket;
            Day_book = day_book;
            Month_book = month_book;
            Year_book = year_book;
            Payment = payment;
        }
        public Reader(string name, string surname, int year, int month, int day, int
       ticket, int day_book, int month_book, int year_book) : base(name, surname, year,
       month, day)
        {
            Ticket = ticket;
            Day_book = day_book;
            Month_book = month_book;
            Year_book = year_book;
            Payment = 50;
        }
        public Reader(Reader obj)
        {
            Ticket = obj.Ticket;
            Day_book = obj.Day_book;
            Month_book = obj.Month_book;
            Year_book = obj.Year_book;
            Payment = obj.Payment;
        }
        public override void ReadInfo()
        {
            base.ReadInfo();
            int tmp = 0;
            Console.Write("Введіть номер читацького квитка:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть номер читацького квитка:");
            }
            Ticket = tmp;
            Console.WriteLine("Введіть дату видачі книги:");
            Console.Write("Введіть рік:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp >
           2021)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть рік:");
            }
            Year_book = tmp;
            Console.Write("Введіть місяць:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp > 13)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть місяць:");
            }
            Month_book = tmp;
            Console.Write("Введіть день:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp > 31)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть день:");
            }
            Day_book = tmp;
            double t;
            Console.Write("Введіть розмір щомісячного читацького внеску:");
            while (!double.TryParse(Console.ReadLine(), out t))
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть розмір щомісячного читацького внеску:");
            }
            Payment = t;
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"\t\tНомер читацького квитка:\t\t{Ticket}");
            Console.WriteLine($"\t\tДата видачі книги:\t\t{Day_book:00}.{ Month_book: 00}.{ Year_book }");
            Console.WriteLine($"\t\tРозмір щомісячного читацького внеску:\t\t{Payment}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("---------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
    


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarytask1
{
    public class Abiturient:Person
    {
        private int zno;
        public int Zno
        {
            set { zno = value; }
            get { return zno; }
        }
        private int marks;
        public int Marks
        {
            set { marks = value; }
            get { return marks; }            
        }
        private string school;
        public string School
        {
            set { school = value; }
            get { return school; }
        }
        public Abiturient() : base()
        {
            Zno = 0;
            Marks = 0;
            School = "Unknown";
        }
        public Abiturient(string name, string surname, int zno, int marks, string
       school, int year, int month, int day) : base(name, surname, year, month, day)
        {
            Zno = zno;
            Marks = marks;
            School = school;
        }
        public Abiturient(string name, string surname, string school, int year, int
       month, int day) : base(name, surname, year, month, day)
        {
            Zno = 4;
            Marks = 10;
            School = school;
        }
        public Abiturient(Abiturient obj)
        {
            Zno = obj.Zno;
            Marks = obj.Marks;
            School = obj.School;
        }
        public override void ReadInfo()
        {
            base.ReadInfo();
            int tmp = 0;
            Console.Write("Введіть кількість балів сертифікатів ЗНО:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть кількість балів сертифікатів ЗНО:");
            }
            Zno = tmp;
            Console.Write("Введіть кількість балів за документ про освіту:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.ResetColor();
                Console.Write("Введіть кількість балів за документ про освіту:");
            }
            Marks = tmp;
            Console.Write("Введіть назву загальноосвітнього навчального закладу:");
            School = Console.ReadLine();
        }
        public override void ShowInfo()
        {
            base.ShowInfo();
            Console.WriteLine($"\t\tКількість балів сертифікатів ЗНО:\t\t{Zno}");
            Console.WriteLine($"\t\tКількість балів за документ про освіту:\t\t{Marks}");
            Console.WriteLine($"\t\tШкола:\t\t{School}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("---------------------------------------------------------------------------------------");            
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
        
    

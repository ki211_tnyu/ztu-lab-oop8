﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrarytask1
{
    public class Person
    {
        private string name;
        public string Name
        {
            set { name = value; }
            get { return name; }
        }
        private string surname;
        public string Surname
        {
            set { surname = value; }
            get { return surname; }
        }
        private int day;
        public int Day
        {
            set { day = value; }
            get { return day; }
        }
        private int month;
        public int Month
        {
            set { month = value; }
            get { return month; }
        }
        private int year;
        public int Year
        {
            set { year = value; }
            get { return year; }
        }
        public Person()
        {
            Name = "Без імені";
            Surname = "Без прізвища";
            Day = 1;
            Month = 1;
            Year = 1999;
        }
        public Person(string name, string surname)
        {
            Name = name;
            Surname = surname;
            Day = 1;
            Month = 1;
            Year = 1999;
        }
        public Person(string name, string surname, int year, int month, int day)
        {
            Name = name;
            Surname = surname;
            Day = day;
            Month = month;
            Year = year;
        }
        public Person(Person obj)
        {
            Name = obj.Name;
            Surname = obj.Surname;
            Day = obj.Day;
            Month = obj.Month;
            Year = obj.Year;
        }
        public virtual void ReadInfo()
        {
            int tmp;
            Person people = new Person();
            Console.Write("Введіть ім'я:"); Name = Console.ReadLine();
            Console.Write("Введіть прізвище:"); Surname = Console.ReadLine();
            Console.WriteLine("Дата народження");
            Console.Write("Введіть рік:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp >
           2021)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть рік:");
            }
            Year = tmp;
            Console.Write("Введіть місяць:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp > 13)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть місяць:");
            }
            Month = tmp; 
            Console.Write("Введіть день:");
            while (!int.TryParse(Console.ReadLine(), out tmp) || tmp < 0 || tmp > 32)
            {
                Console.WriteLine("Помилка! Спробуйте ще раз!");
                Console.Write("Введіть день:");
            }
            Day = tmp;
        }
        public virtual void ShowInfo()
        {
            Console.WriteLine($"\t\tІм'я:\t\t{Name}");
            Console.WriteLine($"\t\tПрізвище:\t\t{Surname}");
            Console.WriteLine($"\t\tДата народження:\t\t{Day:00}.{Month:00}.{Year}");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("---------------------------------------------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
        

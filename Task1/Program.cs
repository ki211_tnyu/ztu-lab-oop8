﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrarytask1;
namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Globalization.CultureInfo customCulture =
           (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Clear();
            Console.Title = "Лабораторна робота №8\nТишкевич Н.Ю КІ-21-1(2)";

            Console.SetWindowSize(100, 25);
            Person person = new Person();
            Teacher teacher = new Teacher();
            Student student = new Student();
            Reader reader = new Reader();
            Abiturient abiturient = new Abiturient();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть поля класа Person:");
            Console.ForegroundColor = ConsoleColor.Black; person.ReadInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть поля класа Abiturient:");
            Console.ForegroundColor = ConsoleColor.Black; abiturient.ReadInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть поля класа Student:");
            Console.ForegroundColor = ConsoleColor.Black; student.ReadInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть поля класа Teacher:");
            Console.ForegroundColor = ConsoleColor.Black; teacher.ReadInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Заповніть поля класа Reader:");
            Console.ForegroundColor = ConsoleColor.Black; reader.ReadInfo();
            Console.WriteLine("\n---------------------------------------------------------------------------\nВивід на екран всієї інформації: "); 
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вивід полей класа Person:");
            Console.ForegroundColor = ConsoleColor.Black; person.ShowInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вивід полей класа Abiturient:");
            Console.ForegroundColor = ConsoleColor.Black; abiturient.ShowInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вивід полей класа Student:");
            Console.ForegroundColor = ConsoleColor.Black; student.ShowInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вивід полей класа Teacher:");
            Console.ForegroundColor = ConsoleColor.Black; teacher.ShowInfo();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Вивід полей класа Reader:");
            Console.ForegroundColor = ConsoleColor.Black; reader.ShowInfo();
        }
    }
}
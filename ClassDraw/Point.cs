﻿using System.Drawing;
using System.Drawing.Drawing2D;
namespace ClassDraw
{
    public class Point : Shape
    {
        protected int x1, y1, width;
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int Width { get; set; }
        public Point(Color color, Color color2, int opacity, int x1,int y1) : base(color, color2, opacity)
        {
            this.x1 = x1;
            this.y1 = y1;
        }
        public Point() : base()
        {
            x1 = rnd.Next(400);
            y1 = rnd.Next(400);
            width = rnd.Next(8);
        }
        public override void Draw(Graphics g)
        {
            Brush brush = new LinearGradientBrush(new System.Drawing.Rectangle(0, 0, 600, 600), Color.FromArgb(opacity, color), Color.FromArgb(opacity, color2), LinearGradientMode.ForwardDiagonal);
            g.FillEllipse(brush, x1, y1, width, width);
        }
    }
}


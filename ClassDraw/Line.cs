﻿using System.Drawing;
using System.Drawing.Drawing2D;
namespace ClassDraw
{
    public class Line : Point
    {
        protected int x2, y2;
        public int X2 { get; set; }
        public int Y2 { get; set; }
        public Line() : base()
        {
            x2 = rnd.Next(700);
            y2 = rnd.Next(700);
            x1 = rnd.Next(800);
            y1 = rnd.Next(800);
        }
        public Line(Color color, Color color2, int opacity, int x1,
       int y1, int x2, int y2) : base(color, color2, opacity, x1, y1)
        {
            this.x2 = x2;
            this.y2 = y2;
        }
        public override void Draw(Graphics g)
        {
            Brush brush = new LinearGradientBrush(new System.Drawing.Rectangle(0, 0, 600, 600), Color.FromArgb(opacity, color), Color.FromArgb(opacity, color2), LinearGradientMode.ForwardDiagonal);
            Pen pen = new Pen(brush, width);
            g.DrawLine(pen, x1, y1, x2, y2);
        }
    }
}

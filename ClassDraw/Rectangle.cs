﻿using System.Drawing;
using System.Drawing.Drawing2D;
namespace ClassDraw
{
    public class Rectangle : Point
    {
        protected int height;
        public int Height { get; set; }
        public Rectangle() : base()
        {
            height = rnd.Next(400);
            width = rnd.Next(400);
        }
        public Rectangle(Color color, Color color2, int opacity,
       int x1, int y1, int height) : base(color, color2, opacity, x1, y1)
        {
            this.height = height;
        }
        public override void Draw(Graphics g)
        {
            Brush brush = new LinearGradientBrush(new System.Drawing.Rectangle(0, 0, 600, 600), Color.FromArgb(opacity, color), Color.FromArgb(opacity, color2), LinearGradientMode.ForwardDiagonal);
            g.FillRectangle(brush, x1, y1, width, height);
        }
    }
}
﻿using System.Drawing;
using System.Drawing.Drawing2D;
namespace ClassDraw
{
    public class Circle : Point
    {
        protected int radius;
        public int Radius { get; set; }
        public Circle() : base()
        {
            radius = rnd.Next(400);
        }
        public Circle(Color color, Color color2, int opacity, int x1,
       int y1, int radius) : base(color, color2, opacity, x1, y1)
        {
            this.radius = radius;
        }
        public override void Draw(Graphics g)
        {
            Brush brush = new LinearGradientBrush(new System.Drawing.Rectangle(0, 0, 600, 600), Color.FromArgb(opacity, color), Color.FromArgb(opacity, color2), LinearGradientMode.ForwardDiagonal);
            g.FillEllipse(brush, x1, y1, radius, radius);
        }
    }
}


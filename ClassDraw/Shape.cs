﻿using System;
using System.Drawing;
namespace ClassDraw
{
    public abstract class Shape
    {
        public static Random rnd = new Random();
        protected static Color color;
        protected static Color color2;
        protected static int opacity;
        public Color Color { set; get; }
        public Color Color2 { set; get; }
        public int Opacity { set; get; }
        public Shape(Color color, Color color2, int opacity)
        {
            Shape.color2 = color2;
            Shape.color = color;
            Shape.opacity = opacity;
        }
        public Shape(Color color, int opacity)
        {
            Shape.color = color;
            Shape.opacity = opacity;
        }
        public Shape()
        {
            opacity = rnd.Next(256);
            color = Color.FromArgb(opacity, rnd.Next(256),
           rnd.Next(256), rnd.Next(256));
            opacity = rnd.Next(256);
            color2 = Color.FromArgb(opacity, rnd.Next(256),
           rnd.Next(256), rnd.Next(256));
        }
        public Shape(Shape obj)
        {
            Color = obj.Color;
            Color2 = obj.Color2;
            Opacity = obj.Opacity;
        }
        public abstract void Draw(Graphics g);
    }
}

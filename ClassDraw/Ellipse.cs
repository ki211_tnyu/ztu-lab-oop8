﻿using System.Drawing;
using System.Drawing.Drawing2D;
namespace ClassDraw
{
    public class Ellipse : Circle
    {
        protected int radius2;
        public int Radius2 { get; set; }
        public Ellipse() : base()
        {
            radius2 = rnd.Next(400);
        }
        public Ellipse(Color color, Color color2, int opacity, int
       x1, int y1, int radius, int Radius2) : base(color, color2, opacity, x1, y1, radius)
        {
            this.radius2 = radius;
        }
        public override void Draw(Graphics g)
        {
            Brush brush = new LinearGradientBrush(new System.Drawing.Rectangle(0, 0, 600, 600), Color.FromArgb(opacity, color), Color.FromArgb(opacity, color2), LinearGradientMode.ForwardDiagonal);
            g.FillEllipse(brush, x1, y1, base.radius, radius2);
        }
    }
}


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassDraw;
namespace Task2
{
    public partial class Form1 : Form
    {
        protected Graphics g;
        protected Random rnd = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonDraw_Click(object sender, EventArgs e)
        {
            Draws();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            pictureBoxPaint.Image = null;
        }
        private void Draws()
        {
            g = pictureBoxPaint.CreateGraphics();
            Shape[] array = new Shape[30];
            for (int i = 0; i < array.Length; i++)
            {
                switch (rnd.Next(6))
                {
                    case 1:
                        array[i] = new ClassDraw.Rectangle();
                        array[i].Draw(g);
                        break;
                    case 2:
                        array[i] = new Circle();
                        array[i].Draw(g);
                        break;
                    case 3:
                        array[i] = new Ellipse();
                        array[i].Draw(g);
                        break;
                    case 4:
                        array[i] = new Line();
                        array[i].Draw(g);
                        break;
                    case 5:
                        array[0] = new ClassDraw.Point();
                        array[0].Draw(g);
                        break;
                }
            }
        }

        private void pictureBoxPaint_Click(object sender, EventArgs e)
        {

        }
    }
}